﻿using System.Threading.Tasks;

namespace tv.abstractions
{
    public interface IMacrosPlayer
    {
        Task PlayAsync(string macrosName);
    }
}
