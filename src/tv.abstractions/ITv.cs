﻿using System.Threading.Tasks;

namespace tv.abstractions
{
    public interface ITv
    {
        Task PowerOnAsync();
        Task PowerOffAsync();
        Task VolUpAsync(int n);
        Task VolDownAsync(int n);
        Task VolMuteAsync();
        Task PauseAsync();
        Task PlayAsync();

        Task MenuAsync();
        Task UpAsync();
        Task DownAsync();
        Task LeftAsync();
        Task RightAsync();
        Task BackAsync();
        Task OkAsync();
        Task InfoAsync();
    }
}
