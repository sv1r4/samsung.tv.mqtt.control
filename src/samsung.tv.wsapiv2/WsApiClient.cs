﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Polly;
using samsung.tv.api.common;
using samsung.tv.wsapiv2.socket;

namespace samsung.tv.wsapiv2
{
    public class WsApiClient:ISamsungApiClient, IDisposable
    {
        private readonly IAsyncWebSocket _ws;
        private readonly ILogger _logger;
        private readonly AsyncPolicy _sendKeyRetryPolicy;

        public WsApiClient(ILogger<WsApiClient> logger, IAsyncWebSocket ws)
        {
            _logger = logger;
            _ws = ws;
            _sendKeyRetryPolicy = InitSendKeyPolicy(logger);
        }

        private static AsyncPolicy InitSendKeyPolicy(ILogger logger)
        {
            const int sendKeyRetryCount = 3;
            
            return Policy.Handle<Exception>().RetryAsync(
                retryCount: sendKeyRetryCount,
                onRetry: (exception, retries, context) =>
                {
                    logger.LogWarning(exception, "Send key retry #{retries}", retries);
                });
        }

    

        public async Task SendKeyAsync(string key, int n)
        {
            await _sendKeyRetryPolicy.ExecuteAsync(() => SendKeyAsyncRaw(key, n));
        }

        private async Task SendKeyAsyncRaw(string key, int n)
        {
            await _ws.ConnectAsync();

            for (var i = 0; i < n; i++)
            {
                await _ws.SendAsync($@"{{
""method"": ""ms.remote.control"",
            ""params"": {{
                ""Cmd"": ""Click"",
                ""DataOfCmd"": ""{key}"",
                ""Option"": ""false"",
                ""TypeOfRemote"": ""SendRemoteKey""
            }}
}}");
            }

        }

        public void Dispose()
        {
            _ws.Dispose();
        }
    }
}
