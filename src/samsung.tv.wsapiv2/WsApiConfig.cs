﻿namespace samsung.tv.wsapiv2
{
    public class WsApiConfig
    {
        public static readonly string RemoteName = "SamsungTvRemote";
        public string Ip { get; set; }
        public int Port { get; set; }
        public int TimeoutConnectMs { get; set; } = 300;
        public int DelaySendMs { get; set; } = 300;
        public string Uri => $"ws://{Ip}:{Port}/api/v2/channels/samsung.remote.control?name={RemoteName}";

    }
}
