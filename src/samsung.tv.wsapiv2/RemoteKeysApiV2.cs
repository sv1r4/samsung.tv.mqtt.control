﻿using samsung.tv.api.common;

namespace samsung.tv.wsapiv2
{

    public class RemoteKeysApiV2 : BaseRemoteKeys
    {
        // ReSharper disable StringLiteralTypo
        public override string KeyPower => "KEY_POWER";
        // ReSharper restore StringLiteralTypo
    }
}
