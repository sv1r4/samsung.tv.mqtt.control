﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebSocketSharp;

namespace samsung.tv.wsapiv2.socket
{
    public class WebSocketSharpAsyncSocket:IAsyncWebSocket
    {
        private WebSocket _ws;
        
        private readonly WsOptions _apiConfig;
        private readonly ILogger<WebSocketSharpAsyncSocket> _logger;

        public WebSocketSharpAsyncSocket(IOptions<WsOptions> apiConfig, ILogger<WebSocketSharpAsyncSocket> logger)
        {
            _logger = logger;
            _apiConfig = apiConfig.Value;

        }
        private void InitNewWebSocketInstance(TaskCompletionSource<object> tcs)
        {
            _ws = new WebSocket(_apiConfig.Uri);
            _ws.OnOpen += async (sender, e) =>
            {
                _logger.LogInformation("Socket opened");
                await Task.Delay(_apiConfig.DelaySend);
                tcs.TrySetResult(null);
            };
            _ws.OnClose += (sender, e) => { _logger.LogInformation("Socket closed"); };
            _ws.OnError += (sender, e) => { HandleSocketException(e.Exception); };
        }

        public async Task ConnectAsync()
        {
            if (_ws?.IsAlive == true)
            {
                return;
            }

            using var cts = new CancellationTokenSource(_apiConfig.TimeoutConnect);
            var tcs = new TaskCompletionSource<object>();

            cts.Token.Register(() => tcs.TrySetCanceled());

            if (_ws == null)
            {
                InitNewWebSocketInstance(tcs);
            }

            if (_ws.ReadyState != WebSocketState.Open)
            {
                try
                {
                    // ReSharper disable once MethodHasAsyncOverload
                    //ConnectAsync raises 'PlatformNotSupportedException' 
                    _ws.Connect();
                }
                catch (Exception ex)
                {
                    HandleSocketException(ex);
                    tcs.TrySetException(ex);
                }
            }
            await tcs.Task;
        }

        private static readonly Func<Exception, bool>[] RecreatePredicates =
        {
            e => e is IOException ioException &&
                 ioException.Message.Contains("fatal", StringComparison.InvariantCultureIgnoreCase),
            e => e is PlatformNotSupportedException,
            e => e is TaskCanceledException
        };

        //todo refactor exception handling
        //throw exception and allow client recreate/reinit object
        //or provide better automatic WebSocket health management
        private void HandleSocketException(Exception e)
        {
            if (RecreatePredicates.Any(p=>p(e)))
            {
                _logger.LogError(e, "Fatal socket error. socket object will be destroyed");
                _ws.Close(CloseStatusCode.Normal);
                _ws = null;
                return;
            }

            _logger.LogError(e, "unhandled socket error event occurred");
        }

        public async Task SendAsync(string data)
        {
            _ws.Send(data);
            //wait some time for the data to be sent becuse WebSocket.Send - non blocking
            await Task.Delay(_apiConfig.DelaySend);
        }

        public void Dispose()
        {
            _ws?.Close();
        }
    }
}
