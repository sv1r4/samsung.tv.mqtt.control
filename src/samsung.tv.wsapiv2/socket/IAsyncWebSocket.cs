﻿using System;
using System.Threading.Tasks;

namespace samsung.tv.wsapiv2.socket
{
    public interface IAsyncWebSocket: IDisposable
    {
        public Task ConnectAsync();
        public Task SendAsync(string data);
    }
}
