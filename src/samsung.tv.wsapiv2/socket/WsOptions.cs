﻿using System;

namespace samsung.tv.wsapiv2.socket
{
    public class WsOptions
    {
        public string Uri { get; set; }
        public TimeSpan TimeoutConnect { get; set; }
        public TimeSpan DelaySend{ get; set; }
    }
}
