﻿namespace samsung.tv.api.common
{
    public abstract class BaseRemoteKeys: IRemoteKeys
    {
        // ReSharper disable StringLiteralTypo
        public abstract string KeyPower { get; }
        public string KeyVolUp => "KEY_VOLUP";
        public string KeyVolDown => "KEY_VOLDOWN";
        public string KeyMute => "KEY_MUTE";
        public string KeyPlay => "KEY_PLAY";
        public string KeyPause => "KEY_PAUSE";
        public string KeyMenu => "KEY_MENU";
        public string KeyBack=> "KEY_RETURN";
        public string KeyOk => "KEY_ENTER";
        public string KeyLeft => "KEY_LEFT";
        public string KeyRight => "KEY_RIGHT";
        public string KeyUp => "KEY_UP";
        public string KeyDown => "KEY_DOWN";
        public string KeyInfo => "KEY_INFO";
        // ReSharper restore StringLiteralTypo
    }
}
