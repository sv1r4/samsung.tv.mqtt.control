﻿namespace samsung.tv.api.common
{
    public interface IRemoteKeys
    {
        string KeyPower { get; }
        string KeyVolUp { get; }
        string KeyVolDown { get; }
        string KeyMute { get; }
        string KeyPlay { get; }
        string KeyPause { get; }
        string KeyMenu { get; }
        string KeyBack { get; }
        string KeyOk { get; }
        string KeyLeft { get; }
        string KeyRight { get; }
        string KeyUp { get; }
        string KeyDown { get; }
        string KeyInfo { get; }
    }
}
