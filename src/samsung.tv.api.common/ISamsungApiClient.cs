﻿using System.Threading.Tasks;

namespace samsung.tv.api.common
{
    public interface ISamsungApiClient
    {
        Task SendKeyAsync(string key, int n);
    }
}
