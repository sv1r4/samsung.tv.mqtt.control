﻿using System.Threading.Tasks;

namespace samsung.tv.wow
{
    public interface IWoWSender
    {
        Task SendAsync(MacAddress mac);
    }
}
