﻿using System;
using System.Globalization;

namespace samsung.tv.wow
{
    public class MacAddress
    {
        private const int Size = 6;
        public byte[] MacBytes { get; }

        protected MacAddress(byte[] macBytes)
        {
            MacBytes = macBytes;
        }

        

        public static MacAddress Parse(string macString)
        {
            var normalized = macString.Replace(":", "").Replace(" ", "");
            if (normalized.Length != Size * 2)
            {
                throw new ArgumentException("invalid MAC address ");
            }

            var b = new byte[Size];
            var j = 0;
            for (var i = 0; i < Size*2; i+=2)
            {
                b[j] = (byte) int.Parse(normalized.Substring(i, 2), NumberStyles.HexNumber);
                j++;
            }
            return new MacAddress(b);
        }
    }
}
