﻿using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace samsung.tv.wow
{
    public class WoWSender: IWoWSender
    {
        private readonly WoWConfig _wowConfig;
        private const int Port = 2014;

        public WoWSender(IOptions<WoWConfig> wowConfig)
        {
            _wowConfig = wowConfig.Value;
        }

        public async Task SendAsync(MacAddress mac)
        {
            var packet = BuildPacket(mac);

            using var udp = new Socket(SocketType.Dgram, ProtocolType.Udp)
            {
                EnableBroadcast = true,
                ExclusiveAddressUse = false
            };
            udp.Bind(new IPEndPoint(IPAddress.Parse(_wowConfig.SenderIp), Port));
            await udp.ConnectAsync("255.255.255.255", Port);
            
            await udp.SendAsync(packet, SocketFlags.None);
            udp.Close();
        }

        private static byte[] BuildPacket(MacAddress mac)
        {
            var packet = new List<byte>();

            packet.AddRange(new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF });
            for (var i = 0; i < 16; i++)
            {
                packet.AddRange(mac.MacBytes);
            }

            packet.AddRange(new byte[] { 0, 0, 0, 0, 0, 0 });
            packet.AddRange(Encoding.ASCII.GetBytes(new[] { 'S', 'E', 'C', 'W', 'O', 'W' }));
            packet.AddRange(new byte[] { 0, 0, 0, 0, 0, 0 });

            return packet.ToArray();
        }
    }
}
