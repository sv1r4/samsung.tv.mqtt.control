﻿using Microsoft.Extensions.Options;
using samsung.tv.wow;
using System;
using System.Threading.Tasks;
using samsung.tv.api.common;
using tv.abstractions;

namespace samsung.tv
{
    public class SamsungTv:ITv
    {
        private const int NPowerOffSend = 3;
        private const int NPowerOnSend = 3;
        private static readonly TimeSpan DelayPowerOnSend = TimeSpan.FromMilliseconds(100);
        private readonly ISamsungApiClient _api;
        private readonly IWoWSender _wow;
        private readonly TvConfig _tvConfig;
        private readonly IRemoteKeys _keys;

        public SamsungTv(ISamsungApiClient api, IWoWSender wow, IOptions<TvConfig> tvConfig, IRemoteKeys keys)
        {
            _api = api;
            _wow = wow;
            _keys = keys;
            _tvConfig = tvConfig.Value;
        }

        public async Task PowerOnAsync()
        {
            for (var i = 0; i < NPowerOnSend; i++)
            {
                await _wow.SendAsync(MacAddress.Parse(_tvConfig.Mac));
                await Task.Delay(DelayPowerOnSend);
            }
        }

        public async Task PowerOffAsync()
        {
            await _api.SendKeyAsync(_keys.KeyPower, NPowerOffSend);
        }

        public Task VolUpAsync(int n)
        {
            return _api.SendKeyAsync(_keys.KeyVolUp, n);
        }

        public Task VolDownAsync(int n)
        {
            return _api.SendKeyAsync(_keys.KeyVolDown, n);
        }

        public Task PauseAsync()
        {
            return _api.SendKeyAsync(_keys.KeyPause, 1);
        }

        public Task PlayAsync()
        {
            return _api.SendKeyAsync(_keys.KeyPlay, 1);
        }

        public Task MenuAsync()
        {
            return _api.SendKeyAsync(_keys.KeyMenu,1);
        }

        public Task UpAsync()
        {
            return _api.SendKeyAsync(_keys.KeyUp, 1);
        }

        public Task DownAsync()
        {
            return _api.SendKeyAsync(_keys.KeyDown, 1);
        }

        public Task LeftAsync()
        {
            return _api.SendKeyAsync(_keys.KeyLeft, 1);
        }

        public Task RightAsync()
        {
            return _api.SendKeyAsync(_keys.KeyRight, 1);
        }

        public Task BackAsync()
        {
            return _api.SendKeyAsync(_keys.KeyBack, 1);
        }
        
        public Task OkAsync()
        {
            return _api.SendKeyAsync(_keys.KeyOk, 1);
        }

        public Task InfoAsync()
        {
            return _api.SendKeyAsync(_keys.KeyInfo, 1);
        }

        public Task VolMuteAsync()
        {
            return _api.SendKeyAsync(_keys.KeyMute, 1);
        }
    }
}
