﻿using samsung.tv.api.common;

namespace samsung.tv
{
    public class TvConfig
    {
        public string Mac { get; set; }
        /// <summary>
        /// for legacy api only
        /// </summary>
        public string ClientMac { get; set; }
        public string Ip { get; set; }
        public int Port { get; set; } = 8001;
        public string SubscribeTopic { get; set; }
        public ApiMode ApiMode { get; set; } = ApiMode.Ws;
    }
}
