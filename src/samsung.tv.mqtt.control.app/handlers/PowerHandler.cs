﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using mqtt.abstractions;
using tv.abstractions;

namespace samsung.tv.mqtt.control.app.handlers
{
    public class PowerHandler:IMqttMessageHandler
    {
        private readonly ITv _tv;
        private readonly ILogger _logger;

        public PowerHandler(ITv tv, ILogger<PowerHandler> logger)
        {
            _tv = tv;
            _logger = logger;
        }

        public bool CanHandle(string topic)
        {
            return topic.EndsWith("/power");
        }

        public async Task HandleAsync(MqttMessage msg)
        {
            if (string.Equals("0", msg.Payload, StringComparison.OrdinalIgnoreCase))
            {
                await _tv.PowerOffAsync();
                _logger.LogInformation("Power off");
            }
            else if (string.Equals("1", msg.Payload, StringComparison.OrdinalIgnoreCase))
            {
                await _tv.PowerOnAsync();
                _logger.LogInformation("Power on");
            }
        }
    }
}
