﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using mqtt.abstractions;
using IMacrosPlayer = samsung.tv.mqtt.control.app.macros.IMacrosPlayer;

namespace samsung.tv.mqtt.control.app.handlers
{
    public class MacrosHandler:IMqttMessageHandler
    {
        private readonly IMacrosPlayer _macrosPlayer;
        private readonly ILogger _logger;

        public MacrosHandler(ILogger<MacrosHandler> logger, IMacrosPlayer macrosPlayer)
        {
            _logger = logger;
            _macrosPlayer = macrosPlayer;
        }

        public bool CanHandle(string topic)
        {
            return topic.EndsWith("/macros", StringComparison.OrdinalIgnoreCase)
                ;
        }

      

        public async Task HandleAsync(MqttMessage msg)
        {
            try
            {
                await _macrosPlayer.RunAsync(msg.Payload);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error playback macros named '{macrosName}'. {errorMessage}", msg.Payload, ex.Message);
                throw;
            }
        }
    }
}
