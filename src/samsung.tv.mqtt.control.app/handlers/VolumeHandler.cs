﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using mqtt.abstractions;
using tv.abstractions;

namespace samsung.tv.mqtt.control.app.handlers
{
    public class VolumeHandler:IMqttMessageHandler
    {
        private readonly ITv _tv;
        private readonly ILogger _logger;

        public VolumeHandler(ITv tv, ILogger<VolumeHandler> logger)
        {
            _tv = tv;
            _logger = logger;
        }

        public bool CanHandle(string topic)
        {
            return topic.EndsWith("/vol", StringComparison.OrdinalIgnoreCase)
                || topic.EndsWith("/vol/up", StringComparison.OrdinalIgnoreCase)
                || topic.EndsWith("/vol/down", StringComparison.OrdinalIgnoreCase)
                || topic.EndsWith("/vol/mute", StringComparison.OrdinalIgnoreCase)
                ;
        }

        public async Task HandleAsync(MqttMessage msg)
        {
            if (await HandleCommonTopicAsync(msg)) return;


            int n;
            if (msg.Topic.EndsWith("/up", StringComparison.OrdinalIgnoreCase))
            {
                int.TryParse(msg.Payload, out n);
                await _tv.VolUpAsync(n);
                _logger.LogInformation("Volume up {times}", n);
            }
            else if (msg.Topic.EndsWith("/down", StringComparison.OrdinalIgnoreCase))
            {
                int.TryParse(msg.Payload, out n);
                await _tv.VolDownAsync(n);
                _logger.LogInformation("Volume down {times}", n);
            }
            else if (msg.Topic.EndsWith("/mute", StringComparison.OrdinalIgnoreCase))
            {
                await _tv.VolMuteAsync();
                _logger.LogInformation("Volume mute");
            }
           
        }

        private async Task<bool> HandleCommonTopicAsync(MqttMessage msg)
        {
            if (msg.Topic.EndsWith("/vol", StringComparison.OrdinalIgnoreCase))
            {
                if (string.Equals("up", msg.Payload, StringComparison.OrdinalIgnoreCase))
                {
                    await _tv.VolUpAsync(1);
                    _logger.LogInformation("Volume up");
                }
                else if (string.Equals("down", msg.Payload, StringComparison.OrdinalIgnoreCase))
                {
                    await _tv.VolDownAsync(1);
                    _logger.LogInformation("Volume down");
                }
                else if (string.Equals("mute", msg.Payload, StringComparison.OrdinalIgnoreCase))
                {
                    await _tv.VolMuteAsync();
                    _logger.LogInformation("Volume mute");
                }

                return true;
            }

            return false;
        }
    }
}
