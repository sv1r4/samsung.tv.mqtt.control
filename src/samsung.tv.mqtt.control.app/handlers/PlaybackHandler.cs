﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using mqtt.abstractions;
using tv.abstractions;

namespace samsung.tv.mqtt.control.app.handlers
{
    public class PlaybackHandler:IMqttMessageHandler
    {
        private readonly ITv _tv;
        private readonly ILogger _logger;

        public PlaybackHandler(ITv tv, ILogger<PlaybackHandler> logger)
        {
            _tv = tv;
            _logger = logger;
        }

        public bool CanHandle(string topic)
        {
            return topic.EndsWith("/playback", StringComparison.OrdinalIgnoreCase)
                || topic.EndsWith("/playback/play", StringComparison.OrdinalIgnoreCase)
                || topic.EndsWith("/playback/pause", StringComparison.OrdinalIgnoreCase)
                ;
        }

        private readonly List<Func<MqttMessage, bool>> _playPredicates = new List<Func<MqttMessage, bool>>
        {
            m =>  m.Topic.EndsWith("/playback", StringComparison.OrdinalIgnoreCase)
                       && string.Equals("play", m.Payload, StringComparison.OrdinalIgnoreCase),
            m =>  m.Topic.EndsWith("/playback/play", StringComparison.OrdinalIgnoreCase)
        };

        private readonly List<Func<MqttMessage, bool>> _pausePredicates = new List<Func<MqttMessage, bool>>
        {
            m =>  m.Topic.EndsWith("/playback", StringComparison.OrdinalIgnoreCase)
                  && string.Equals("pause", m.Payload, StringComparison.OrdinalIgnoreCase),
            m =>  m.Topic.EndsWith("/playback/pause", StringComparison.OrdinalIgnoreCase)
        };

        public async Task HandleAsync(MqttMessage msg)
        {
            if (_playPredicates.Any(x => x(msg)))
            {
                await _tv.PlayAsync();
                _logger.LogInformation("Play btn press");
            }else if (_pausePredicates.Any(x => x(msg)))
            {
                await _tv.PauseAsync();
                _logger.LogInformation("Pause btn press");
            }

           
        }
    }
}
