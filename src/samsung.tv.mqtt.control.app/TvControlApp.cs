﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using mqtt.abstractions;

namespace samsung.tv.mqtt.control.app
{
    public class TvControlApp:IHostedService
    {
        private readonly IMqttAdapter _mqtt;
        private readonly TvConfig _config;
        public TvControlApp(IMqttAdapter mqtt, IOptions<TvConfig> config)
        {
            _mqtt = mqtt;
            _config = config.Value;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await _mqtt.ConnectAsync();
            await _mqtt.SubscribeAsync(_config.SubscribeTopic);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return _mqtt.DisconnectAsync();
        }
    }
}
