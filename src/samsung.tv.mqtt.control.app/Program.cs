﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using mqtt.abstractions;
using mqtt.mqttnet;
using MQTTnet;
using MQTTnet.Extensions.ManagedClient;
using samsung.tv.api.common;
using samsung.tv.legacyapi;
using samsung.tv.mqtt.control.app.handlers;
using samsung.tv.mqtt.control.app.macros;
using samsung.tv.wow;
using samsung.tv.wsapiv2;
using samsung.tv.wsapiv2.socket;
using tv.abstractions;
using IMacrosPlayer = samsung.tv.mqtt.control.app.macros.IMacrosPlayer;

namespace samsung.tv.mqtt.control.app
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            using var host = CreateHostBuilder(args).Build();

            await host.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseConsoleLifetime()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<TvControlApp>();
                    
                    var tvSec = hostContext.Configuration.GetSection(nameof(TvConfig));
                    var tvConfig = tvSec.Get<TvConfig>();
                    services.Configure<TvConfig>(tvSec);
                    services.Configure<MqttConfig>(hostContext.Configuration.GetSection(nameof(MqttConfig)));
                    switch (tvConfig.ApiMode)
                    {
                        case ApiMode.Ws:
                            services.Configure<WsOptions>(options =>
                            {
                                var apiConfig = new WsApiConfig
                                {
                                    Ip = tvConfig.Ip,
                                    Port = tvConfig.Port
                                };

                                hostContext.Configuration.GetSection(nameof(WsApiConfig)).Bind(apiConfig);
                                options.Uri = apiConfig.Uri;
                                options.DelaySend = TimeSpan.FromMilliseconds(apiConfig.DelaySendMs);
                                options.TimeoutConnect = TimeSpan.FromMilliseconds(apiConfig.TimeoutConnectMs);

                            });
                            services.AddSingleton<IAsyncWebSocket, WebSocketSharpAsyncSocket>();
                            services.AddSingleton<ISamsungApiClient, WsApiClient>();
                            services.AddSingleton<IRemoteKeys, RemoteKeysApiV2>();
                            break;
                        case ApiMode.Tcp:
                            services.Configure<TcpApiConfig>(config =>
                            {
                                hostContext.Configuration.GetSection(nameof(TcpApiConfig)).Bind(config);
                                config.Ip = tvConfig.Ip;
                                config.Port = tvConfig.Port;
                                config.ClientMac = tvConfig.ClientMac;

                            });
                            services.AddSingleton<ISamsungApiClient, TcpApiClient>();
                            services.AddSingleton<IRemoteKeys, RemoteKeysLegacy>();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    
                    services.Configure<WoWConfig>(hostContext.Configuration.GetSection(nameof(WoWConfig)));

                    
                    services.AddSingleton<IWoWSender, WoWSender>();
                    services.AddTransient<IMqttAdapter, MqttNetAdapter>();
                    services.AddSingleton<ITv, SamsungTv>();
                    services.AddSingleton<IMacrosPlayer, ConfigMacrosPlayer>();
                    services.AddSingleton(new MqttFactory().CreateManagedMqttClient());

                    services.Scan(scan => scan
                        .FromAssemblyOf<PowerHandler>()
                        .AddClasses(classes => classes.AssignableTo<IMqttMessageHandler>())
                        .AsImplementedInterfaces()
                        .WithTransientLifetime());
                });
    }
}
