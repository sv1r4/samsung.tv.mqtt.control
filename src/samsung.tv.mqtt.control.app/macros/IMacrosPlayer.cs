﻿using System.Threading.Tasks;

namespace samsung.tv.mqtt.control.app.macros
{
    public interface IMacrosPlayer
    {
        Task RunAsync(string macrosName);
    }
}
