﻿#nullable enable
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using tv.abstractions;

namespace samsung.tv.mqtt.control.app.macros
{
    public class MacrosSequence:IEnumerable<Func<Task>>
    {
        private readonly ITv _tv;
     
        private readonly string[] _rawItems;
        public MacrosSequence( ITv tv, string[] rawSequenceItems)
        {
            _rawItems = rawSequenceItems;
            _tv = tv;
        }

        public IEnumerator<Func<Task>> GetEnumerator()
        {
            return new MacrosEnumerator(_tv, _rawItems);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new MacrosEnumerator(_tv, _rawItems);
        }
    }

    public class MacrosEnumerator: IEnumerator<Func<Task>>
    {
        private readonly ITv _tv;
        public static readonly string PrefixDelay = "d_";
        private readonly IEnumerator _rawItemsEnumerator;
        private static readonly IReadOnlyDictionary<string, Func<ITv, Task>> MapCommands =
            new Dictionary<string, Func<ITv, Task>>
            {
                {"up",  tv => tv.UpAsync()},
                {"down", tv => tv.DownAsync()},
                {"left", tv => tv.LeftAsync()},
                {"right", tv => tv.RightAsync()},
                {"ok", tv => tv.OkAsync()},
                {"menu", tv => tv.MenuAsync()},
                {"back", tv => tv.BackAsync()},
                {"info", tv => tv.InfoAsync()},
            };

        
        public static IEnumerable<string> SupportedCommands => MapCommands.Keys;
        

        public MacrosEnumerator(ITv tv, string[] rawItems)
        {
            _tv = tv;
            _rawItemsEnumerator = rawItems.GetEnumerator();
        }

        public bool MoveNext()
        {
            return _rawItemsEnumerator.MoveNext();
        }

        public void Reset()
        {
            _rawItemsEnumerator.Reset();
        }


        public Func<Task> Current => () =>
        {
            var rawItem = _rawItemsEnumerator.Current as string;
            if (string.IsNullOrWhiteSpace(rawItem))
            {
                return Task.CompletedTask;
            }
            if (rawItem.StartsWith(PrefixDelay))
            {
                var delayMs = int.Parse(rawItem.Substring(PrefixDelay.Length));
                return Task.Delay(delayMs);
            }
            if (int.TryParse(rawItem, out var rawDelayMs))
            {
                return Task.Delay(rawDelayMs);
            }

            if(MapCommands.TryGetValue(rawItem, out var cmd))
            {
                return cmd(_tv);
            }
            return Task.CompletedTask;
        };

        object? IEnumerator.Current => Current;

        public void Dispose()
        {
            
        }
    }
}
