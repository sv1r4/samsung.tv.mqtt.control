﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using tv.abstractions;

namespace samsung.tv.mqtt.control.app.macros
{
    public class ConfigMacrosPlayer:IMacrosPlayer
    {
        private const string MacrosSection = "Macroses";
        public static readonly char MacrosItemsSeparator = ' ';
        private readonly Lazy<Dictionary<string, MacrosSequence>> _macroses;

        public ConfigMacrosPlayer(ITv tv, IConfiguration configuration)
        {
            _macroses = new Lazy<Dictionary<string, MacrosSequence>>(() =>
            {
                var macrosConfigs = configuration.GetSection(MacrosSection).Get<List<MacrosConfig>>();
                return macrosConfigs.ToDictionary(
                    x => x.Name,
                    x =>new MacrosSequence(tv, x.Data.Split(MacrosItemsSeparator, StringSplitOptions.RemoveEmptyEntries)));
            });
        }

        public async Task RunAsync(string macrosName)
        {
            if(!_macroses.Value.TryGetValue(macrosName, out var sequence))
            {
                throw new InvalidOperationException($"no macros with name '{macrosName}' found");
            }
            var errors = new List<Exception>();
            foreach (var task in sequence)
            {
                try
                {
                    await task();
                }
                catch (Exception ex)
                {
                    errors.Add(ex);
                }
            }

            if (errors.Any())
            {
                throw new AggregateException(errors);
            }
        }
    }
}
