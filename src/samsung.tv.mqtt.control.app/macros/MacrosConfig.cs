﻿
namespace samsung.tv.mqtt.control.app.macros
{
    public class MacrosConfig
    {
        public string Name { get; set; }
        /// <summary>
        /// d_(delay time in ms) : d_1000 - for delay in 1 sec
        /// separator - ' '  (space)
        /// commands: up down left right ok back menu
        /// example macros command sequence: 'menu d_1000 down ok d_1500 back back back'
        /// </summary>
        public string Data { get; set; }
    }
}
