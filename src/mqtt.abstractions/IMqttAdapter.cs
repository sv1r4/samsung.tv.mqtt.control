﻿using System.Threading.Tasks;

namespace mqtt.abstractions
{
    public interface IMqttAdapter
    {
        Task ConnectAsync();
        Task DisconnectAsync();
        Task SubscribeAsync(string topic);
    }
}