﻿using System.Threading.Tasks;

namespace mqtt.abstractions
{
    public interface IMqttMessageHandler
    {
        bool CanHandle(string topic);

        Task HandleAsync(MqttMessage msg);
    }
}
