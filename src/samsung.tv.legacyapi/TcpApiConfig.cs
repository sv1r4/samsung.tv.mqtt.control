﻿namespace samsung.tv.legacyapi
{
    public class TcpApiConfig
    {
        public string Ip { get; set; }
        public int Port { get; set; } = 55000;
        public string ClientMac { get; set; }
        public int SendCommandDelayMs { get; set; } = 200;
    }
}
