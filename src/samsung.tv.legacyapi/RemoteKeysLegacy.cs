﻿using samsung.tv.api.common;

namespace samsung.tv.legacyapi
{
    public class RemoteKeysLegacy : BaseRemoteKeys
    {
        // ReSharper disable StringLiteralTypo
        public override string KeyPower => "KEY_POWEROFF";
        // ReSharper restore StringLiteralTypo
    }
}
