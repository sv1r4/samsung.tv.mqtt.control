﻿using System.Net.Sockets;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using samsung.tv.api.common;

namespace samsung.tv.legacyapi
{
    public class TcpApiClient:ISamsungApiClient
    {
        private readonly TcpApiConfig _tcpApiConfig;
        private const string ClientName = "tcp.remote";
        private const string AppName1 = "iphone..iapp.samsung";
        private const string AppName2 = "iphone.UN60D6000.iapp.samsung";

        public TcpApiClient(IOptions<TcpApiConfig> tcpApiConfig)
        {
            _tcpApiConfig = tcpApiConfig.Value;
        }

        public async Task ConnectAsync()
        {
            using var tcp = new TcpClient(_tcpApiConfig.Ip, _tcpApiConfig.Port);
            var s = tcp.GetStream();

            using var messageBuilder = new TcpMessageBuilder(128);

            var message = messageBuilder
                .AddRange(0x64, 0x00)
                .AddBase64AsciiStringWithCountEncoded(_tcpApiConfig.Ip)
                .AddBase64AsciiStringWithCountEncoded(_tcpApiConfig.ClientMac)
                .AddBase64AsciiStringWithCountEncoded(ClientName)
                .Build()
                ;
            
            using var commandBuilder = new TcpMessageBuilder(512);

            
            var command = commandBuilder.AddRange(0x00, (byte)AppName1.Length, 0x00)
                    .AddAsciiString(AppName1)
                    .AddRange((byte)message.Length, 0x00)
                    .AddRange(message)
                    .Build()
                ;
            

            await s.WriteAsync(command, 0, command.Length);
        }

        private async Task SendKeyAsync(string key)
        {
            using var tcp = new TcpClient(_tcpApiConfig.Ip, _tcpApiConfig.Port);
            var s = tcp.GetStream();
            using var messageBuilder = new TcpMessageBuilder(32);

            var message = messageBuilder
                .AddRange(0x00, 0x00, 0x00)
                .AddBase64AsciiStringWithCountEncoded(key)
                .Build();
            
            using var commandBuilder = new TcpMessageBuilder(128);
            

            var command = commandBuilder
                .AddRange(0x00, (byte)AppName2.Length, 0x00)
                .AddAsciiString(AppName2)
                .Add((byte)message.Length)
                .Add(0x00)
                .AddRange(message)
                .Build()
                ;


            await s.WriteAsync(command, 0, command.Length);
        }

        public async Task SendKeyAsync(string key, int n)
        {
            await ConnectAsync();
            for (var i = 0; i < n; i++)
            {
                await SendKeyAsync(key);
                await Task.Delay(_tcpApiConfig.SendCommandDelayMs);
            }
        }
    }
}
