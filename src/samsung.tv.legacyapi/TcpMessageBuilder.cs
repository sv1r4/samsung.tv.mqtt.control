﻿using System;
using System.Buffers;
using System.Text;

namespace samsung.tv.legacyapi
{
    public class TcpMessageBuilder:IDisposable
    {
        //todo validations
        private readonly char[] _buffer;
        private int _size;
        private int _position = 0;
        private byte[] _resultBuf;

        public TcpMessageBuilder(int size)
        {
            _size = size;
            _buffer = ArrayPool<char>.Shared.Rent(size);
        }

        public TcpMessageBuilder Add(char ch)
        {
            return Add(ch);
        }

        public TcpMessageBuilder Add(byte b)
        {
            _buffer[_position] = (char)b;
            _position++;
            return this;
        }

        public TcpMessageBuilder AddRange(params byte[] bytes)
        {
            foreach (var b in bytes)
            {
                Add(b);
            }
            return this;
        }

      

        public TcpMessageBuilder AddAsciiString(string str)
        {
            var sBytes = Encoding.ASCII.GetBytes(str);
            foreach (var b in sBytes)
            {
                Add(b);
            }

            return this;
        }

        public TcpMessageBuilder AddBase64AsciiStringWithCountEncoded(string str)
        {
            var sBytes = Encoding.ASCII.GetBytes(str);
            var p = _position;
            var cnt = Convert.ToBase64CharArray(sBytes, 0, sBytes.Length, _buffer, _position + 2 );
            _buffer[p] = (char) cnt;
            _buffer[p + 1] = (char)0x00;
            _position += cnt+2;
            return this;
        }

        public byte[] Build()
        {
            _resultBuf = ArrayPool<byte>.Shared.Rent(_position);
            for (var i =0;i<_position;i++)
            {
                _resultBuf[i] = (byte) _buffer[i];
            }
            return _resultBuf;
        }

        public void Dispose()
        {
            if (_resultBuf != null)
            {
                ArrayPool<byte>.Shared.Return(_resultBuf);
            }
            if (_resultBuf != null)
            {
                ArrayPool<char>.Shared.Return(_buffer);
            }
        }
    }
}
