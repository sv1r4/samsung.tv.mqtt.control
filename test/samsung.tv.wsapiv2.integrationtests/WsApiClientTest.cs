using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using samsung.tv.api.common;
using samsung.tv.wsapiv2.socket;
using Xunit;

namespace samsung.tv.wsapiv2.integrationtests
{
    public class WsApiClientTest
    {
        private readonly IRemoteKeys _keys;
        private readonly WsApiClient _client;

        public WsApiClientTest()
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            var wsApiConfig = new WsApiConfig(); configuration.GetSection(nameof(WsApiConfig)).Bind(wsApiConfig);

            var wsConfig = new WsOptions
            {
                Uri = wsApiConfig.Uri,
                DelaySend = TimeSpan.FromMilliseconds(wsApiConfig.DelaySendMs),
                TimeoutConnect = TimeSpan.FromMilliseconds(wsApiConfig.TimeoutConnectMs)
            };

            IOptions<WsOptions> wsOptions = new OptionsWrapper<WsOptions>(wsConfig);
            _keys = new RemoteKeysApiV2();
            var stubLoggerClient = new Mock<ILogger<WsApiClient>>(MockBehavior.Loose).Object;
            var stubLoggerSocket = new Mock<ILogger<WebSocketSharpAsyncSocket>>(MockBehavior.Loose).Object;

            _client = new WsApiClient(stubLoggerClient, new WebSocketSharpAsyncSocket(wsOptions, stubLoggerSocket));
        }

        [Fact]
        public async Task ApiV2Client_SendPower_TvSwitchOff()
        {
            await _client.SendKeyAsync(_keys.KeyPower, 2);
        }

        [Fact]
        public async Task ApiV2Client_SendVolDown_VolumeChanged()
        {
            await _client.SendKeyAsync(_keys.KeyVolDown,2);
        }

        

    }
}
