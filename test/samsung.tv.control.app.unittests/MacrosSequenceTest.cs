using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using samsung.tv.control.app.unittests.extensions;
using samsung.tv.mqtt.control.app.macros;
using tv.abstractions;
using Xunit;

namespace samsung.tv.control.app.unittests
{
    public class MacrosSequenceTest
    {
        private readonly IMock<ITv> _stubTv = new Mock<ITv>(MockBehavior.Loose);
        
        private const double Precision = 0.15;

        [Theory]
        [InlineData(new[]{"d_100","menu","down"}, 3)]
        [InlineData(new[]{"d_500","menu"}, 2)]
        [InlineData(new []{"d_500","menu  "}, 2)]
        [InlineData(new[]{"500","menu"}, 2)]
        [InlineData(new[]{"d_500","back","back","back"}, 4)]
        [InlineData(new[]{"d_500"}, 1)]
        public void ValidSequence_Count_ExpectedItemsCount(string[] rawMacrosData, int expectedCount)
        {
            var ms = new MacrosSequence(_stubTv.Object, rawMacrosData);

            Assert.Equal(expectedCount, ms.Count());
        }

        [Theory]
        [InlineData(1)]
        [InlineData(3)]
        public async Task SingleDelaySequence_Foreach_SequenceExecutionDelayed(int delaySeconds)
        {
            var sw = new Stopwatch();
            var msDelay = delaySeconds * 1000;
            var ms = new MacrosSequence(_stubTv.Object, new[]{$"d_{msDelay}"});
            sw.Start();
            foreach (var t in ms)
            {
                await t();
            }
            sw.Stop();
            sw.Elapsed.AssertTimespanEqualWithPrecision(msDelay, Precision);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(3)]
        public async Task SingleDelaySequenceNoPrefix_Foreach_SequenceExecutionDelayed(int delaySeconds)
        {
            var sw = new Stopwatch();
            var msDelay = delaySeconds * 1000;
            var ms = new MacrosSequence(_stubTv.Object, new []{$"{msDelay}"});
            sw.Start();
            foreach (var t in ms)
            {
                await t();
            }
            sw.Stop();
            sw.Elapsed.AssertTimespanEqualWithPrecision(msDelay, Precision);
        }

        [Theory]
        [InlineData(new[]{1,2})]
        [InlineData(new[]{1,1,1})]
        [InlineData(new[]{1,2,3})]
        public async Task MultipleDelaySequence_Foreach_SequenceExecutionDelayed(int[] delaySeconds)
        {
            var sw = new Stopwatch();
            var msDelay = delaySeconds.Sum() * 1000;
            var ms = new MacrosSequence(_stubTv.Object, delaySeconds.Select(d=>$"d_{d*1000}").ToArray());
            sw.Start();
            foreach (var t in ms)
            {
                await t();
            }
            sw.Stop();
            sw.Elapsed.AssertTimespanEqualWithPrecision(msDelay, Precision);
        }

        [Theory]
        [InlineData(new[]{1,2})]
        [InlineData(new[]{1,1,1})]
        [InlineData(new[]{1,2,3})]
        public async Task MultipleDelaySequenceNoPrefix_Foreach_SequenceExecutionDelayed(int[] delaySeconds)
        {
            var sw = new Stopwatch();
            var msDelay = delaySeconds.Sum() * 1000;
            var ms = new MacrosSequence(_stubTv.Object, delaySeconds.Select(d=>$"{d*1000}").ToArray());
            sw.Start();
            foreach (var t in ms)
            {
                await t();
            }
            sw.Stop();
            sw.Elapsed.AssertTimespanEqualWithPrecision(msDelay, Precision);
        }

        [Theory]
        [MemberData(nameof(ValidSequence_Foreach_ProperCommandsCalled_Data))]
        public async Task ValidSequence_Foreach_ProperCommandsCalled(string[] rawMacrosData, IDictionary<string, int> counts)
        {
            var mockTv = new Mock<ITv>(MockBehavior.Loose);
          
            var ms = new MacrosSequence(mockTv.Object, rawMacrosData);

            foreach (var t in ms)
            {
                await t();
            }

            foreach (var count in counts)
            {
                Assert.Equal(count.Value, mockTv.Invocations.Count(x=>x.Method.Name == count.Key));
            }
            
        }

        public static IEnumerable<object[]> ValidSequence_Foreach_ProperCommandsCalled_Data()
        {
            yield return new object[]
            {
                new[]{"menu","up","up","up","ok","down","back","back"},
                new Dictionary<string, int>
                {
                    {nameof(ITv.MenuAsync), 1},
                    {nameof(ITv.UpAsync), 3},
                    {nameof(ITv.OkAsync), 1},
                    {nameof(ITv.DownAsync), 1},
                    {nameof(ITv.BackAsync), 2},
                }
            };

            yield return new object[]
            {
                new[]{"menu","d_100","up","up","up","ok","d_200","down","back","back"},
                new Dictionary<string, int>
                {
                    {nameof(ITv.MenuAsync), 1},
                    {nameof(ITv.UpAsync), 3},
                    {nameof(ITv.OkAsync), 1},
                    {nameof(ITv.DownAsync), 1},
                    {nameof(ITv.BackAsync), 2},
                }
            };

            yield return new object[]
            {
                new []{"menu","up","left","right","down","back","back","ok"},
                new Dictionary<string, int>
                {
                    {nameof(ITv.MenuAsync), 1},
                    {nameof(ITv.UpAsync), 1},
                    {nameof(ITv.LeftAsync), 1},
                    {nameof(ITv.RightAsync), 1},
                    {nameof(ITv.DownAsync), 1},
                    {nameof(ITv.BackAsync), 2},
                    {nameof(ITv.OkAsync), 1},
                }
            };
        }
    }
}
