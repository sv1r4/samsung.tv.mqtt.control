﻿using System;
using Xunit;

namespace samsung.tv.control.app.unittests.extensions
{
    public static class AssertExtension
    {
        public static void AssertTimespanEqualWithPrecision(this TimeSpan actual, int expectedMs, double precision)
        {
            
            Assert.True(expectedMs*(1+precision) > actual.TotalMilliseconds);
            Assert.True(expectedMs*(1-precision) < actual.TotalMilliseconds);
        }
    }
}
