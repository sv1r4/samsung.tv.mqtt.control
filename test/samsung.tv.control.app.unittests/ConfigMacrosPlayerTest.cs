﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Moq;
using samsung.tv.mqtt.control.app.macros;
using tv.abstractions;
using Xunit;

namespace samsung.tv.control.app.unittests
{
    public class ConfigMacrosPlayerTest
    {
        private readonly IConfiguration _configuration;
        private readonly Mock<ITv> _stubTv;
        private const string MacrosNameExists1 = "testMacrosNameExists1";
        private const string MacrosNameExists2 = "testMacrosNameExists2";
        private const string MacrosNameNotExists1 = "testMacrosNameNotExists1";
        public ConfigMacrosPlayerTest()
        {
            var configurationBuilder = new ConfigurationBuilder()
                .AddInMemoryCollection(new []
                {
                    new KeyValuePair<string, string>("Macroses:0:Name", MacrosNameExists1),
                    new KeyValuePair<string, string>("Macroses:0:Data", "d_100"),
                    new KeyValuePair<string, string>("Macroses:1:Name", MacrosNameExists2),
                    new KeyValuePair<string, string>("Macroses:1:Data", "d_100"),
                });
            _configuration = configurationBuilder.Build();

            _stubTv = new Mock<ITv>(MockBehavior.Loose);
        }

        [Fact]
        public async Task ConfigSectionNotExists_RunAsync_ExceptionThrown()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var configuration = configurationBuilder.Build();
            var stubTv = new Mock<ITv>(MockBehavior.Loose);
            
            var cmp = new ConfigMacrosPlayer(stubTv.Object,configuration);
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await cmp.RunAsync("any"));
        }

        [Theory]
        [InlineData(MacrosNameNotExists1)]
        public async Task MacrosNotExists_RunAsync_ExceptionThrown(string macrosName)
        {
            var cmp = new ConfigMacrosPlayer(_stubTv.Object, _configuration);
            var ex = await Assert.ThrowsAsync<InvalidOperationException>(async () => await cmp.RunAsync(macrosName));
            Assert.Contains(macrosName, ex.Message);
            Assert.Contains("no macros", ex.Message);
        }

        [Theory]
        [InlineData(MacrosNameExists1)]
        [InlineData(MacrosNameExists2)]

        public async Task ConfigSectionExistsMacrosExists_RunAsync_NoException(string macrosName)
        {
            var cmp = new ConfigMacrosPlayer(_stubTv.Object, _configuration);

            await cmp.RunAsync(macrosName);
        }

        [Theory]
        [InlineData(MacrosNameExists1)]
        [InlineData(MacrosNameExists2)]

        public async Task MacrosExists_RunAsync_NoErrors(string macrosName)
        {
            var cmp = new ConfigMacrosPlayer(_stubTv.Object, _configuration);

            await cmp.RunAsync(macrosName);
        }

        [Theory]
        [InlineData(
            "up d_1000 down d_1000 left d_1000 right back back",
            new[] {nameof(ITv.UpAsync), nameof(ITv.DownAsync), nameof(ITv.LeftAsync), nameof(ITv.RightAsync), nameof(ITv.BackAsync), nameof(ITv.BackAsync)})]
        [InlineData(
            "up ok",
            new[] {nameof(ITv.UpAsync), nameof(ITv.OkAsync)})]
        [InlineData(
            " up  ok  ",
            new[] {nameof(ITv.UpAsync), nameof(ITv.OkAsync)})]
        [InlineData(
            "ok ok back ok ok back back",
            new[] {nameof(ITv.OkAsync), nameof(ITv.OkAsync), nameof(ITv.BackAsync), nameof(ITv.OkAsync), nameof(ITv.OkAsync), nameof(ITv.BackAsync), nameof(ITv.BackAsync)})]

        public async Task MacrosWithValidDataExists_RunAsync_ProperTvCommandsCalledInProvidedOrder(string macrosData, string[] expectedInvocations)
        {
            var mockTv = new Mock<ITv>(MockBehavior.Loose);
            var macrosName = "testMacros3";
            
            var configurationBuilder = new ConfigurationBuilder()
                .AddInMemoryCollection(new []
                {
                    new KeyValuePair<string, string>("Macroses:0:Name", macrosName),
                    new KeyValuePair<string, string>("Macroses:0:Data", macrosData),
                });

            var cmp = new ConfigMacrosPlayer(mockTv.Object, configurationBuilder.Build());

            await cmp.RunAsync(macrosName);

            Assert.Equal(expectedInvocations.Length, mockTv.Invocations.Count);
           
            for (var i = 0; i < expectedInvocations.Length; i++)
            {
                Assert.Equal(expectedInvocations[i], mockTv.Invocations[i].Method.Name);
            }

            
        }
    }
}
