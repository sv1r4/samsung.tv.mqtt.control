using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Xunit;

namespace samsung.tv.wow.integrationtests
{
    public class WoWSenderTest
    {
        private readonly IOptions<WoWConfig> _wowOptions;
        
        public WoWSenderTest()
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            var wowConfig = configuration.GetSection(nameof(WoWConfig)).Get<WoWConfig>();
            _wowOptions = new OptionsWrapper<WoWConfig>(wowConfig);
        }

        [Theory]
        [InlineData("C0:97:27:4C:AC:46")]
        [InlineData("14:49:E0:00:C9:7F")]
        public async Task WowSender_SendAsync_TvSwitchedOn(string mac)
        {
            const int nPowerOnSend = 10;
            var wow = new WoWSender(_wowOptions);

            for (var i = 0; i < nPowerOnSend; i++)
            {
                await wow.SendAsync(MacAddress.Parse(mac));
                await Task.Delay(200);
            }

        }
    }
}
