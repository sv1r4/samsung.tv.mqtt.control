using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using samsung.tv.api.common;
using Xunit;

namespace samsung.tv.legacyapi.integrationtests
{
    public class TcApiClientTest
    {
        private readonly IOptions<TcpApiConfig> _tcpApiOptions;
        private readonly IRemoteKeys _keys;

        public TcApiClientTest()
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            var tcpApiConfig = configuration.GetSection(nameof(TcpApiConfig)).Get<TcpApiConfig>();
            _tcpApiOptions = new OptionsWrapper<TcpApiConfig>(tcpApiConfig);
            _keys = new RemoteKeysLegacy();
        }

        [Fact]
        public async Task TcpApi_ConnectAsync_SocketConnected()
        {
            var c = new TcpApiClient(_tcpApiOptions);
            //tv prompt shows first time for specific client mac address
            await c.ConnectAsync();
        }

        [Fact]
        public async Task TcpApi_SendPowerKey_TvSwitchedOff()
        {
            var c = new TcpApiClient(_tcpApiOptions);
            await c.SendKeyAsync(_keys.KeyPower, 2);
        }

        [Fact]
        public async Task TcpApi_SendVolumeDownKey_TvVolumeChanged()
        {
            var c = new TcpApiClient(_tcpApiOptions);
            await c.SendKeyAsync(_keys.KeyVolDown, 5);
        }
    }
}
